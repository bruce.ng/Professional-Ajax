<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Autosuggest</title>
    <script type="text/javascript" src="<%=request.getContextPath()%>/Chapter08-json/Autosuggest/json.js"></script>
    <script type="text/javascript" src="zxml.js"></script>
    <script type="text/javascript" src="autosuggest.js"></script>
    
    <link rel="stylesheet" type="text/css" href="autosuggest.css" />
    <script type="text/javascript">
        window.onload = function () {
            var oTextbox = new AutoSuggestControl(document.getElementById("txtState"), new SuggestionProvider());
        }
/**
This line creates a new AutoSuggestControlobject,
passing a reference to the textbox with the ID of txtStateand a new SuggestionProvider() class.
It’s important that this line be executed in the onload event handler
because document.getElementById() isn’t 100 percent accurate until the entire page has been loaded.
*/
    </script>
</head>
<body>
    <form method="post" onsubmit="alert('Submitted!')">
        <table border="0">
            <tr>
                <td>Name:</td>
                <td><input type="text" name="txtName" id="txtName" /></td>
            </tr>
            <tr>
                <td>Address 1:</td>
                <td><input type="text" name="txtAddress1" id="txtAddress1" /></td>
            </tr>
            <tr>
                <td>Address 2:</td>
                <td><input type="text" name="txtAddress2" id="txtAddress2" /></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><input type="text" name="txtCity" id="txtCity" /></td>
            </tr>
            <tr>
                <td>State/Province:</td>
                <!-- <td><input type="text" name="txtState" id="txtState" autocomplete="on" /></td> -->
                <td><input type="text" name="txtState" id="txtState" /></td>
            </tr>
            <tr>
                <td>Zip Code:</td>
                <td><input type="text" name="txtZip" id="txtZip" /></td>
            </tr>
            <tr>
                <td>Country:</td>
                <td><input type="text" name="txtCountry" id=" txtCountry" /></td>
            </tr>
        </table>
        <input type="submit" value="Save Information" />
    </form>
</body>
</html>
