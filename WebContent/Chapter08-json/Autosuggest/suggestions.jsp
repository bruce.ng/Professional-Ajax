<%@page contentType="text/plain" pageEncoding="UTF-8" import="java.sql.*, java.io.*, org.json.*"%>
<%
    //get the raw input
    BufferedReader reader = request.getReader();
    StringBuffer buffer = new StringBuffer();
    String line = "";
    while ((line = reader.readLine()) != null){
        buffer.append(line);
        buffer.append("\n");
    }
    reader.close();
    String input = buffer.toString().trim();
    
    //construct the JSON object
    JSONObject data = new JSONObject(input);
    
    //database information
    String dbservername = "localhost";
    String dbname = "ProAjax";
    String username = "root";
    String password = "0000";
    String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;
    
    //query
    String sql = "Select * from " + data.getString("requesting") + " where Name like '" + data.getString("text") + "%' order by Name ASC limit 0," + data.getString("limit");
    
    //resultset
    JSONArray matches = new JSONArray();
    
    try {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        
        //create database connection
        Connection conn = DriverManager.getConnection(url);
        
        //execute query
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        boolean found = rs.next();
        
        //if there was a match...
        if (found) {
            
            do {
                matches.put(rs.getString("Name"));
            } while (rs.next());
        } 
        
        rs.close();
        conn.close();
    } catch (Exception ex){
        //ignore
    }
%><%=matches.toString()%>