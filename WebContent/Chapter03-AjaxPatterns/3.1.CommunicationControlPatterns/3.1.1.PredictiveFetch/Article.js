var oXHR = null;//The XHR object
var iPageCount = 4;//The number of pages
var iCurPage = -1;//The currently displayed page
var iWaitBeforeLoad = 5000;//The time (in ms) before loading new page
var iNextPageToLoad = -1;//The next page to load

/**
 * The first variable is a global XHR object that is used to make all requests for more information.
 * The second, iPageCount, is the number of pages used in this article.
 * (This is hard-coded here, but in actual practice this would have to be generated.).
 * The iCurPagevariable stores the page number currently being displayed to the user.
 * The next two variables deal directly with the preloading of data:
 * iWaitBeforeLoadis the number of milliseconds to wait before loading the next page,
 * and iNextPageToLoadcontains the page number that should be loaded once the specified amount of time has passed.
 * 
 */
function showPage(sPage) {
  
  var divPage = document.getElementById("divPage" + sPage);
  console.log('divPage', divPage);
  if (divPage) {
    for (var i = 0; i < iPageCount; i++) {
      var iPageNum = i + 1;
      var divOtherPage = document.getElementById("divPage" + iPageNum);
      var aOtherLink = document.getElementById("aPage" + iPageNum);
      console.log('divOtherPage', divOtherPage, 'sPage', sPage, 'iPageNum', iPageNum);
      if (divOtherPage && sPage != iPageNum) {
        divOtherPage.style.display = "none";
        aOtherLink.className = "";
      }
    }
    divPage.style.display = "block";
    document.getElementById("aPage" + sPage).className = "current";
  } else {
    //the <div/>element doesn’t exist,he page navigates to the next page in the article
    //the old-fashioned way, by getting the URL (using the getURLForPage()function defined previously)
    //and assigning it to location.href. This is a fallback functionality so that if the user clicks a page link
    //before 5 seconds are up, the experience falls back to the traditional web paradigm.
    location.href = getURLForPage(parseInt(sPage));
  }
}

function getURLForPage(iPage) {
  var sNewUrl = location.href;
  console.log('sNewUrl', sNewUrl);
  console.log('location.search', location.search);
  if (location.search.length > 0) {
    sNewUrl = sNewUrl.substring(0, sNewUrl.indexOf("?"));
  }
  sNewUrl += "?page=" + iPage;
  return sNewUrl;
}

function loadNextPage() {
  
  if (iNextPageToLoad <= iPageCount) {
    
    if (!oXHR) {
      oXHR = zXmlHttp.createRequest();
    } else if (oXHR.readyState != 0) {
      oXHR.abort();
    }
    
    //the open() method is called, specifying that the request will get an asynchronous GET request.
    oXHR.open("get", getURLForPage(iNextPageToLoad) + "&dataonly=true", true);
    
    oXHR.onreadystatechange = function() {
      
      if (oXHR.readyState == 4 && oXHR.status == 200) {
        var divLoadArea = document.getElementById("divLoadArea");
        divLoadArea.innerHTML = oXHR.responseText;
        
        var divNewPage = document.getElementById("divPage" + iNextPageToLoad);
        divNewPage.style.display = "none";
        document.body.appendChild(divNewPage);
        
        divLoadArea.innerHTML = "";
        iNextPageToLoad++;
        console.log('setTimeout', iNextPageToLoad - 1);
        setTimeout(loadNextPage, iWaitBeforeLoad);
      }
    };
    oXHR.send(null);
  }
}

// if Ajax is enabled, assign event handlers and begin fetching
window.onload = function() {
  console.log('window.onload');
  if (zXmlHttp.isSupported()) {
    //alert(location.href);
    //location.href = http://localhost:8080/Professional-Ajax/chapter03/CommunicationControlPatterns/PredictiveFetch/ArticleExample.jsp
    if (location.href.indexOf("page=") > -1) {
      console.log('location.search', location.search);
      var sQueryString = location.search.substring(1);
      console.log('sQueryString', sQueryString);
      iCurPage = parseInt(sQueryString.substring(sQueryString.indexOf("=") + 1));
    } else {
      iCurPage = 1;
    }
    
    iNextPageToLoad = iCurPage + 1;
    
    var colLinks = document.getElementsByTagName("a");
    console.log('colLinks', colLinks);
    
    for (var i = 0; i < colLinks.length; i++) {
      
      console.log('colLinks[i].id', colLinks[i].id);
      
      if (colLinks[i].id.indexOf("aPage") == 0) {
        //event onclick tren Tag <a>
        colLinks[i].onclick = function(oEvent) {
          //this.id = "aPage1"
          var sPage = this.id.substring(5);
          showPage(sPage);
          
          if (oEvent) {
            oEvent.preventDefault();
          } else {
            window.event.returnValue = false;
          }
        };
      }
    }
    console.log('setTimeout', 1);
    setTimeout(loadNextPage, iWaitBeforeLoad);
  }
};