/**
 *A single function called validateField()is used to validate each form field.
 *This is possible because each field uses the same validation technique (call the server and wait for a response).
 *The only differences are the types of data being validated and which image to show if validation is unsuccessful.
 */
function validateField(oEvent) {
  /**
   * The first two lines of code inside this function equalize the differences between event models in IE and
   DOM-compliant browsers (such as Mozilla Firefox, Opera, and Safari).
   DOM-compliant browsers pass in an event object to each event handler;
   the control that caused the event is stored in the eventobject’s target property.
   In IE, the event object is a property of window;
   therefore, the first line inside the function assigns the correct value to the oEventvariable.
   Logical OR (||) returns a non-null value when used with an object and a null object.
   If you are using IE, oEventwill be undefined; thus, the value of window.event is assigned to oEvent.
   If you are using a DOM-compliant browser, oEventwill be reassigned to it self
   * */
  oEvent = oEvent || window.event;
  /**
   The second line does the same operation for the control that caused the event,
   which is stored in the srcElementproperty in IE.
   At the end of these two lines, the control that caused the event is stored in the txtField variable.
   */
  var txtField = oEvent.target || oEvent.srcElement;
  
  /**The next step is to create the HTTP request using XHR:*/
  var oXHR = zXmlHttp.createRequest();
  oXHR.open("get", "ValidateForm.jsp?" + txtField.name + "=" + encodeURIComponent(txtField.value), true);
  
  oXHR.onreadystatechange = function() {
    if (oXHR.readyState == 4) {
      if (oXHR.status == 200) {
        //(oXHR.responseText); = false||This e-mail address is not valid
        var arrInfo = oXHR.responseText.split("||");
        var imgError = document.getElementById("img" + txtField.id.substring(3) + "Error");
        var btnSignUp = document.getElementById("btnSignUp");
        
        if (!eval(arrInfo[0])) {
          imgError.title = arrInfo[1];
          imgError.style.display = "";
          txtField.valid = false;
        } else {
          imgError.style.display = "none";
          txtField.valid = true;
        }
        
        btnSignUp.disabled = !isFormValid();
      } else {
        alert("An error occurred while trying to contact the server.");
      }
    }
  };
  
  oXHR.send(null);
};

function isFormValid() {
  var frmMain = document.forms[0];
  var blnValid = true;
  
  for (var i = 0; i < frmMain.elements.length; i++) {
    if (typeof frmMain.elements[i].valid == "boolean") {
      blnValid = blnValid && frmMain.elements[i].valid;
    }
  }
  
  return blnValid;
}

//if Ajax is enabled, disable the submit button and assign event handlers
window.onload = function() {
  if (zXmlHttp.isSupported()) {
    var btnSignUp = document.getElementById("btnSignUp");
    var txtUsername = document.getElementById("txtUsername");
    var txtBirthday = document.getElementById("txtBirthday");
    var txtEmail = document.getElementById("txtEmail");
    
    btnSignUp.disabled = true;
    txtUsername.onchange = validateField;
    txtBirthday.onchange = validateField;
    txtEmail.onchange = validateField;
    txtUsername.valid = false;
    txtBirthday.valid = false;
    txtEmail.valid = false;
    
  }
};