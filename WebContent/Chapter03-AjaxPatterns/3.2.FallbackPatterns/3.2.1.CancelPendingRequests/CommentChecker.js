/**
 * The comment notification example can be modified to take this into account.
 * This is a case where the Ajax solution provides additional value to the user but is not the primary focus of the page.
 * If a request fails, there is no reason to alert the user; you can simply cancel any future requests to prevent
 * any further errors from occurring.
 * To do so, you must add a global variable that indicates whether requests are enabled:
 * */

var oXHR = null;
var iInterval = 1000;
var iLastCommentId = -1;
var divNotification = null;

/**
 * the blnRequestsEnabledvariable must be checked before any request is made
 * */
var blnRequestsEnabled = true;

function checkComments() {
  
  //gửi requeste get đén file CheckComments.jsp, đồng bộ hóa true,
  //reponse trả về state == 4 status == 200 thì thực thì đoạn code sau
  if (blnRequestsEnabled) {
    try {//TODO --------- chu y
      if (!oXHR) {
        oXHR = zXmlHttp.createRequest();
      } else {
        oXHR.abort();
      }
      
      oXHR.open("get", "CheckComments.jsp", true);
      oXHR.onreadystatechange = function() {//TODO --------- chu y
        
        if (oXHR.readyState == 4) {
          if (oXHR.status == 200 || oXHR.status == 304) {
            
            var aData = oXHR.responseText.split("||");
            if (aData[0] != iLastCommentId) {
              
              iLastCommentId = aData[0];
              
              if (iLastCommentId != -1) {
                showNotification(aData[1], aData[2]);
              }
              
            }
            //Thời gian thoát 1s
            setTimeout(checkComments, iInterval);
          } else {//TODO --------- chu y
            blnRequestsEnabled = false;
          }
        }
      };
      
      oXHR.send(null);
    } catch (oException) {//TODO --------- chu y
      blnRequestsEnabled = false;//TODO --------- chu y
    }
  } //End: if
}

function showNotification(sName, sMessage) {
  if (!divNotification) {
    divNotification = document.createElement("div");
    divNotification.className = "notification";
    document.body.appendChild(divNotification);
  }
  
  divNotification.innerHTML = "<strong>New Comment</strong><br />" + sName + " says: " + sMessage + "...<br /><a href=\"ViewComment.php?id="
      + iLastCommentId + "\">View</a>";
  divNotification.style.top = document.body.scrollTop + "px";
  divNotification.style.left = document.body.scrollLeft + "px";
  divNotification.style.display = "block";
  setTimeout(function() {
    divNotification.style.display = "none";
  }, 5000);
}

//if Ajax is enabled, assign event handlers and begin fetching
window.onload = function() {
  if (zXmlHttp.isSupported()) {
    checkComments();
  }
};