<%@page import="java.awt.*"%>
<%@page import="java.awt.image.*"%>
<%@page import="sun.awt.image.codec.*"%>
<%@page contentType="image/jpeg"%>

<%
  //create the image
  BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
  Graphics2D g = (Graphics2D) image.getGraphics();
  g.setColor(Color.white);
  g.fillRect(0, 0, 1, 1);
  g.dispose();
  
  //output it
  ServletOutputStream output = response.getOutputStream();
  JPEGImageEncoderImpl encoder = new JPEGImageEncoderImpl(output);
  encoder.encode(image);
%>