<%@page contentType="text/plain"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%!
    protected String getCustomerData(int id){
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            //database information
            String dbservername = "localhost";
            String dbname = "ProAjax";
            String username = "root";
            String password = "0000";
            String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;
            
            //create database connection
            Connection conn = DriverManager.getConnection(url);
            
            //execute query
            String sql = "Select * from Customers where CustomerId=" + id;
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            boolean found = rs.next();
            StringBuffer buffer = new StringBuffer();
                
            //if there was a match...
            if (found) {
                
                buffer.append(rs.getString("Name"));
                buffer.append("<br />");                
                buffer.append(rs.getString("Address"));                
                buffer.append("<br />");                
                buffer.append(rs.getString("City"));                
                buffer.append("<br />");                
                buffer.append(rs.getString("State"));                
                buffer.append("<br />");                
                buffer.append(rs.getString("Zip"));                
                buffer.append("<br /><br />");                
                buffer.append("Phone: " + rs.getString("Phone"));                
                buffer.append("<br /><a href=\"mailto:");                
                buffer.append(rs.getString("Email"));                
                buffer.append("\">");                
                buffer.append(rs.getString("Email"));                
                buffer.append("</a>");
            } else {
                buffer.append("Customer with ID ");
                buffer.append(id);
                buffer.append(" could not be found.");
            }
            
            rs.close();
            conn.close();
            
            return buffer.toString();
        } catch (Exception ex){
            return "An error occurred while trying to get customer info.";
        }
        
    }
%>
<%
    String id = request.getParameter("id");
    String callback = request.getParameter("callback");
    String message = "";
    int customerId = -1;
    
    try {
        customerId = Integer.parseInt(id);
        message = getCustomerData(customerId);
    } catch (Exception ex) {
        message = "Invalid customer ID.";
    }
              
%>
<%=message%>