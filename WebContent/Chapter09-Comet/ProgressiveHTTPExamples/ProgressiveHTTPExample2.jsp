<%@page contentType="text/html" pageEncoding="UTF-8" buffer="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Progressive HTTP Example 2</title>
    </head>
    <body>
        <script type="text/javascript">
            document.title = "First message";
        </script>
<% 
    response.flushBuffer();
    Thread.sleep(10000);
%>        
        
        <script type="text/javascript">
            document.title = "Second message";
        </script>
    </body>
</html>
