/**
 * Each of these functions handles a different case and is used as the values of onsuccess, onfailure, 
 * and on notmodified for each request.
 * They each output a message, including the HTTP status, the priority (in parentheses), the request type, the URL,
 * and the age of the request.
 * */

function outputResult(oResponse, sColor, name) {
   var divResponses = document.getElementById("divResponses");
   var oRequest = oResponse.request;
   var sMessage = "<div style=\"background-color:" + sColor + "\">"
            + oResponse.status + " -" + name +" (" + oRequest.priority + ") "
            + oRequest.type + " " + oRequest.url + " " + oRequest.age + "</div>";
   divResponses.innerHTML += sMessage;
}

//prints its message with a white background
function outputSuccessResult(oResponse) {
    outputResult(oResponse, "white", "Success");
}

//uses a red background
function outputFailureResult(oResponse) {
    outputResult(oResponse, "red", "Failure");
}

//silver background
function outputNotModifiedResult(oResponse) {
    outputResult(oResponse, "silver" , "NotModified");
}



/**
 * creates a poll request to poll.txt.
 * which doesn’t exist; this should result in a 404 error.
 */
function addPoll() {
    RequestManager.poll({
        type : "get",
        url : "poll.txt",
        onsuccess : outputSuccessResult,
        onfailure : outputFailureResult,
        onnotmodified : outputNotModifiedResult
    });
}

/**
 * the addSubmit() and addSubmitPart() functions create POST requests to a text file, post.txt,
 * which should fail with a 405 error (most servers won’t let you post data to a plain text file)
 * */
function addSubmit() {
    RequestManager.submit({
        type : "post",
        url : "post.txt",
        data : "name=Nicholas",
        onsuccess : outputSuccessResult,
        onfailure : outputFailureResult,
        onnotmodified : outputNotModifiedResult
    });
}

function addSubmitPart() {
    RequestManager.submitPart({
        type : "post",
        url : "post.txt",
        data : "name=Nicholas",
        onsuccess : outputSuccessResult,
        onfailure : outputFailureResult,
        onnotmodified : outputNotModifiedResult
    });
}

//function creates a GET request for data.txt
function addPreFetch() {
    RequestManager.prefetch({
        type : "get",
        url : "data.txt",
        onsuccess : outputSuccessResult,
        onfailure : outputFailureResult,
        onnotmodified : outputNotModifiedResult
    });
}

/**creates a very low-priority request that should be executed only after everything else has been completed
 * Mức ưu tiền có số càng cao, thì độ ưu tiên càng nhỏ,
 * mức độ ưu tiên của các request 0 > 1 > ..
 */
function addLowPriority() {
    RequestManager.send({
        priority: 10,
        type : "get",
        url : "data.txt",
        onsuccess : outputSuccessResult,
        onerror : outputFailureResult,
        onnotmodified : outputNotModifiedResult
    });
}


window.onload = function () {
    //chạy theo thứ tự ưu tiên
    addPoll();
    addPoll();
    addSubmit();
    addPreFetch();
    addLowPriority();
    addSubmitPart();
    addLowPriority();
    addPreFetch();
    addPoll();
    addSubmit();
    addPoll();
    addPoll();
};