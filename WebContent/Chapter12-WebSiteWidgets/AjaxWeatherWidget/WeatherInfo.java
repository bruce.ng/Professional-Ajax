package wrox.professionalAjax.weather;

import java.util.*;
import java.io.*;
import java.net.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;

public class WeatherInfo {
    String _path;
    String _cachedFile;
    String _license;
    String _partner;
    String _location;
    
    public WeatherInfo(String path, String license, String partner, String location) {
        _path = path;
        _cachedFile = String.format("%s%cweather_cache.xml", _path, File.separatorChar);
        _license = license;
        _partner = partner;
        _location = location;
    }
    
    public String getWeather() {
        Date now = new Date();
        Date fileTime = new Date(_getLastModified());
        
        if (now.compareTo(fileTime) > -1) {
            return _getWebWeather();
        } else {
            return _getCachedWeather();
        }
    }
    
    private String _getWebWeather() {
        try {
            URL url = new URL("http://xoap.weather.com/weather/local/" + _location + "?cc=*&prod=xoap&par=" + _partner
                    + "&key=" + _license + "");
            
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            
            huc.setRequestMethod("GET");
            
            huc.connect();
            
            StreamSource source = new StreamSource(huc.getInputStream());
            
            StreamSource finalStyle = new StreamSource(new File(String.format("%s%cweather.xslt", _path,
                    File.separatorChar)));
            
            StreamResult result = new StreamResult(new File(_cachedFile));
            
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer(finalStyle);
            transformer.transform(source, result);
            
            huc.disconnect();
            
            return _getCachedWeather();
        } catch (Exception ex) {
            try {
                StreamSource source = new StreamSource(new File(String.format("%s%cerror_doc.xml", _path,
                        File.separatorChar)));
                
                StreamSource finalStyle = new StreamSource(new File(String.format("%s%cweather.xslt", _path,
                        File.separatorChar)));
                
                OutputStream outputStream = new ByteArrayOutputStream();
                StreamResult result = new StreamResult(outputStream);
                
                TransformerFactory transFactory = TransformerFactory.newInstance();
                Transformer transformer = transFactory.newTransformer(finalStyle);
                transformer.transform(source, result);
                
                outputStream.close();
                
                return outputStream.toString();
            } catch (Exception e) {
                //do nothing
            }
        }
        return "";
    }
    
    private String _getCachedWeather() {
        File file = new File(_cachedFile);
        
        try {
            byte[] buffer = new byte[4096];
            FileInputStream is = new FileInputStream(file);
            OutputStream outputStream = new ByteArrayOutputStream();
            
            while (true) {
                int read = is.read(buffer);
                
                if (read == -1) {
                    break;
                }
                
                outputStream.write(buffer, 0, read);
            }
            
            outputStream.close();
            is.close();
            
            return outputStream.toString();
        } catch (Exception ex) {
            
        }
        return "";
    }
    
    private long _getLastModified() {
        File file = new File(_cachedFile);
        
        if (file.exists()) {
            return file.lastModified();
        } else {
            return 0;
        }
        
    }
}