<%@page contentType="text/plain" pageEncoding="UTF-8" import="java.sql.*, java.io.*, org.json.*"%>
<%
response.setHeader("Cache-Control","no-cache");

if (request.getParameter("search") != null) {
	String searchTerm = request.getParameter("search");
	
	String dbservername = "localhost";
	String dbname = "BlogDatabase";
	String username = "root";
	String password = "0000";
	String url = "jdbc:mysql://" + dbservername + "/" + dbname + "?user=" + username + "&password=" + password;

	String sql = "SELECT TOP 10 BlogId, Title FROM BlogPosts WHERE Post LIKE '%" + searchTerm + "%' " +
			"OR Title LIKE '%" + searchTerm + "%' ORDER BY Date DESC";

	JSONArray matches = new JSONArray();
	
	try
	{
		Class.forName("com.mysql.jdbc.Driver").newInstance();

        //create database connection
        Connection conn = DriverManager.getConnection(url);

		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		boolean found = rs.next();

		//if there was a match...
		if (found)
		{
			do
			{
				JSONObject jso = new JSONObject();
				jso.put("id", rs.getString("BlogId"));
				jso.put("title", rs.getString("Title"));
				matches.put(jso);
			}
			while (rs.next());
		}

		rs.close();
		conn.close();
	}
	catch (Exception ex)
	{
		//ignore
	}

	out.println(matches.toString());
}
%>