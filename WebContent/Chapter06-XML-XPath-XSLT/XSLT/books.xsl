<?xml version="1.0" encoding="UTF-8" ?>
<!--
XSLT is an XML-based language designed to transform an XML document into another data form.

XSLT documents are nothing more than specialized XML documents, so they must conform to ( conform to : tuần theo)
the same rules as all XML documents: they must contain an XML declaration,
they must have a single root element, and they must be well formed

The document element of an XSLT document is <xsl:stylesheet/>
In this element, the XSL version is specified and the xsl namespace is declared.
This required information determines the behavior of the XSLT processor; without it, an error will be thrown.
The xsl prefix is also important,
as this allows all XSL directives to be visibly and logically separate from other code in the document.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
    The <xsl:output/>element defines the format of the resulting output.
 -->
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes" />
<!--
Just like any application, a transformation must have an entry point.
XSLT is a template-based language, and the processor works on an XML document by matching template rules

The first element to match is the root of the XML document.
This is done by using the <xsl:template/> directive. Directives tell the processor to execute a specific function.
The <xsl:template/> directive creates a template that is used when the pattern in the match attribute is matched
-->
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="ie/books.css" />
                <title>XSL Transformations</title>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
<!--
The match attribute takes an XPath expression to select the proper XML node.
In this case, it is the root element of books.xml (the XPath expression /always selects the root node of the document).

Inside of the template, you’ll notice HTML elements. These elements are a part of the transformation’s output.
Inside of the <body/> element, another XSL directive is found. The <xsl:apply-templates /> element
tells the processor to start parsing all templates within the context of the document element,
which brings the next template into play:
-->
    <xsl:template match="book">
        <div class="bookContainer">
            <xsl:variable name="varIsbn" select="@isbn" />
            <xsl:variable name="varTitle" select="title" />
            <img class="bookCover" alt="{$varTitle}" src="ie/images/{$varIsbn}.png" />
            <div class="bookContent">
                <h3>
                    <xsl:value-of select="$title" />
                </h3>
                Written by:
                <xsl:value-of select="author" />
                <br />
                ISBN #
                <xsl:value-of select="$varIsbn" />
                <div class="bookPublisher">
                    <xsl:value-of select="publisher" />
                </div>
            </div>
        </div>
    </xsl:template>
<!--
This new template matches all <book/> elements,
so when the processor reaches each <book/> in the XML document, this template is used.

-  The first two XSL directives in this template are <xsl:variable/>, which define variables.
   Variables in XSL are primarily used in XPath expressions or attributes
   (where elements cannot be used without breaking XML syntax).

-  The <xsl:variable/> element has two attributes: name and select.
   As you may have guessed,
   - The name attribute sets the name of the variable
   - The select attribute specifies an XPath expression and stores the matching value(s) in the variable.
   
   After the initial declaration, variables are referenced to with the $sign
   (so, the variable defined as varIsbn is later referenced as $varIsbn).
   
   The first variable, $varIsbn, is assigned the value of the <book/>element’s isbn - attribute.
   The second, $varTitle, is assigned the value of the <title/> element.
   
   These two pieces of information are used in the attributes of the HTML <img/> element.
   To output variables in attributes, you surround the variable name in braces:
   
   <img class="bookCover" alt="{$title}" src="images/{$isbn}.png" />
   
   Without the braces, the output would use the string literals “$varTitle” and “$varIsbn” instead.
   
   The remainder of XSL directives in this example are <xsl:value-of/>elements.
   These elements retrieve the value of the matched variable or node according to the select attribute.
   The select attribute behaves in the same way as the select attributes of <xsl:variable/> do: they take an XPath 
   expression and select the node or variable that matches that expression.
   
   The first instance of <xsl:value-of/>in this template references the $varTitle variable (notice the lack of braces),
   so the value of the variable is used. Next, the value of the <author/>element is used;
   the same with $varTitleand <publisher/>.
   
   In order for an XML document to transform in the browser, it must have a stylesheet specified.
   In books.xml, add the following line immediately after the XML declaration
   
   <?xml-stylesheet type="text/xsl" href="books.xsl"?>
   
   This tells the XML processor to apply the stylesheet books.xsl to this document.
   Viewing this modified XML document in a web browser will no longer show the XML structure,
   but it will show the resulting transformation to HTML. However, using this directive won’t work through JavaScript.
   For that, you’ll need to use some special objects
-->
</xsl:stylesheet>
